var WebSocketServer = function (options) {
    require('http').Server.call(this);
    
    this.on('connection', onConnect);
    this.on('request', onRequest);  // HTTP Request Handler
    this.on('upgrade', onUpgrade);  // WebSocket Request Handler
    
    this.WebSocketResponse75 = [
        'HTTP/1.1 101 Web Socket Protocol Handshake', 
        'Upgrade: WebSocket', 
        'Connection: Upgrade',
        'WebSocket-Origin: {origin}',
        'WebSocket-Location: {protocol}://{host}{resource}',
        '',
        ''
    ]
    this.WebSocketResponse76 = [
        'HTTP/1.1 101 Web Socket Protocol Handshake', 
        'Upgrade: WebSocket', 
        'Connection: Upgrade',
        'Sec-WebSocket-Origin: {origin}',
        'Sec-WebSocket-Location: {protocol}://{host}{resource}',
        '',
        '{data}'
    ]
    
    function onConnect() {
    }
    
    function onRequest(req, res) {
        var fileName = '.'
        if (req.url == '/') fileName += '/ParodyWave.html';
        else fileName += req.url;
        require('fs').readFile(fileName, function (err, data) {
            if (err) {
                res.writeHead(404);
                res.end();
                console.log(fileName);
                return;
            } else {
                res.writeHead(200, {'Content-Length': data.length});
                res.write(data);
                res.end();
            }
        });
    }

    function onUpgrade(req, socket, head) {
        if ("sec-websocket-key1", "sec-websocket-key2" in req.headers) { // 76
            var key = calcResponseKey(req.headers["sec-websocket-key1"],
                                    req.headers["sec-websocket-key2"], head);
            if (key) {
                var res = socket.server.WebSocketResponse76.join('\r\n')
                            .replace(/\{origin\}/,req.headers.origin || '')
                            .replace(/\{protocol\}/,'ws')
                            .replace(/\{host\}/,req.headers.host || '')
                            .replace(/\{resource\}/,req.url || '')
                            .replace(/\{data\}/,key);
            } else {
                socket.end();
                return;
            }
        } else {    // 75
            var res = socket.server.WebSocketResponse75.join('\r\n')
                        .replace(/\{origin\}/,req.headers.origin || '')
                        .replace(/\{protocol\}/,'ws')
                        .replace(/\{host\}/,req.headers.host || '')
                        .replace(/\{resource\}/,req.url || '')
        }
        
        try {
            socket.write(res, "binary");
            var client = initiateWebSocket(socket);
            
            var action = require('./ParodyWave');
            action.service(client);
            client.ready(); // "open" event for action
            setTimeout(socket.end, 300 * 1000);
        } catch (e) {
            socket.destroy();
        }
    }

    function calcResponseKey(key1, key2, key) {
        var md5 = require('crypto').createHash('md5');
        [key1, key2].forEach(function (k) {
            var n = parseInt(k.replace(/[^\d]/g,'')),
                space = k.replace(/[^\ ]/g,'').length;
            
            if (space === 0 || n % space !== 0) {
                return null;
            }
            
            n = parseInt(n/space);
            var result = '';
            result += String.fromCharCode(n >> 24 & 0xFF);
            result += String.fromCharCode(n >> 16 & 0xFF);
            result += String.fromCharCode(n >> 8 & 0xFF);
            result += String.fromCharCode(n & 0xFF);
            
            md5.update(result);
        });
        md5.update(key);
        return md5.digest('binary');
    }
    
    function initiateWebSocket(socket) {
        var c = new process.EventEmitter();
        socket.on("data", onData);
        socket.on("end", function () { socket.end(); });
        socket.on("close", onClose);
        socket.on("error", onError);
        socket.wsBuffer = "";
        
        // WebSocket readyState status
        // 0: CONNECTING, 1: OPEN, 2: CLOSING, 3:CLOSED
        c.readyState = 0;
        c.ready = function() {
            if (c.readyState === 0) {
                c.readyState = 1;
                c.emit("open");
            }
        }
        c.write = function (data) {
          try {
            socket.write("\u0000", "binary");
            socket.write(data, "utf8");
            socket.write("\uffff", "binary");
          } catch(e) {
            socket.end();
          }
        };
        
        c.end = function () {
            socket.end();
            c.readyState = 2;
        };
        
        return c;
        
        function onData(data) {
            socket.wsBuffer += data;
            
            var chunks = socket.wsBuffer.split("\ufffd"),
                count = chunks.length - 1;
            
            for (var i = 0 ; i < count ; i ++) {
                var chunk = chunks[i];
                if (chunk[0] == "\u0000") {
                    c.emit("message", chunk.slice(1));
                } else {
                    socket.end();
                    return;
                }
            }
            
            socket.wsBuffer = chunks[count];
        }
        
        function onClose() {
            c.emit("close");
            c.readyState = 2;
        }
        
        function onError(exception) {
            if (c.listeners("error").length > 0) {
                c.emit("error", exception);
            } else {
                throw exception;
            }
        }
    }
};
require("sys").inherits(WebSocketServer, require("http").Server);

var server = new WebSocketServer();
server.listen(8888);