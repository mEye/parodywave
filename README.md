# Parody Wave
Sample project for open seminar on 2010.12.04

## Requirement
* nodeJS

## Etc
* [WebSocket 채팅 프로그램 예제 - 패러디 웨이브 <1>](http://dev.meye.net/entry/WebSocket-%EC%B1%84%ED%8C%85-%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%A8-%EC%98%88%EC%A0%9C-%ED%8C%A8%EB%9F%AC%EB%94%94-%EC%9B%A8%EC%9D%B4%EB%B8%8C-1)
* [WebSocket 채팅 프로그램 예제 - 패러디 웨이브 <2>](http://dev.meye.net/entry/WebSocket-%EC%B1%84%ED%8C%85-%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%A8-%EC%98%88%EC%A0%9C-%ED%8C%A8%EB%9F%AC%EB%94%94-%EC%9B%A8%EC%9D%B4%EB%B8%8C-2)
* [WebSocket 채팅 프로그램 예제 - 패러디 웨이브 <3>](http://dev.meye.net/entry/WebSocket-%EC%B1%84%ED%8C%85-%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%A8-%EC%98%88%EC%A0%9C-%ED%8C%A8%EB%9F%AC%EB%94%94-%EC%9B%A8%EC%9D%B4%EB%B8%8C-3)
* [WebSocket 채팅 프로그램 예제 - 패러디 웨이브 <4>](http://dev.meye.net/entry/WebSocket-%EC%B1%84%ED%8C%85-%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%A8-%EC%98%88%EC%A0%9C-%ED%8C%A8%EB%9F%AC%EB%94%94-%EC%9B%A8%EC%9D%B4%EB%B8%8C-4)
